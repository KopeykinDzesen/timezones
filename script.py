import requests
import pytz
import json
from bs4 import BeautifulSoup


UNUSUAL_TZS = {
    'fields': {'name': 'US/Pacific', 'country': 'United States', 'city': 'Pacific'},
    'fields': {'name': 'US/Mountain', 'country': 'United States', 'city': 'Mountain'},
    'fields': {'name': 'US/Hawaii', 'country': 'United States', 'city': 'Hawaii'},
    'fields': {'name': 'US/Eastern', 'country': 'United States', 'city': 'Eastern'},
    'fields': {'name': 'US/Central', 'country': 'United States', 'city': 'Central'},
    'fields': {'name': 'US/Arizona', 'country': 'United States', 'city': 'Arizona'},
    'fields': {'name': 'US/Alaska', 'country': 'United States', 'city': 'Alaska'},
    'fields': {'name': 'Europe/Saratov', 'country': 'Russia', 'city': 'Saratov'},
    'fields': {'name': 'Europe/Kirov', 'country': 'Russia', 'city': 'Kirov'},
    'fields': {'name': 'Europe/Busingen', 'country': 'Germany', 'city': 'Busingen'},
    'fields': {'name': 'Canada/Pacific', 'country': 'Canada', 'city': 'Pacific'},
    'fields': {'name': 'Canada/Newfoundland', 'country': 'Canada', 'city': 'Newfoundland'},
    'fields': {'name': 'Canada/Mountain', 'country': 'Canada', 'city': 'Mountain'},
    'fields': {'name': 'Canada/Eastern', 'country': 'Canada', 'city': 'Eastern'},
    'fields': {'name': 'Canada/Central', 'country': 'Canada', 'city': 'Central'},
    'fields': {'name': 'Canada/Atlantic', 'country': 'Canada', 'city': 'Atlantic'},
    'fields': {'name': 'Asia/Yangon', 'country': 'Myanmar', 'city': 'Yangon'},
    'fields': {'name': 'Asia/Tomsk', 'country': 'Russia', 'city': 'Tomsk'},
    'fields': {'name': 'Asia/Famagusta', 'country': 'Cyprus', 'city': 'Famagusta'},
    'fields': {'name': 'Asia/Atyrau', 'country': 'Kazakhstan', 'city': 'Atyrau'}
}


def get_cities(timezone):
    responce = requests.get(f'https://www.zeitverschiebung.net/en/timezone/{tz_format(timezone)}')
    soup = BeautifulSoup(responce.text)
    country = get_country(soup)
    soup = soup.find(id="collapseOne")

    if not soup:
        return country, [timezone.split('/')[-1], ]

    cities = [link.get_text() for link in soup.find_all('a')]
    return country, cities


def tz_format(timezone):
    return '--'.join([word.lower() for word in timezone.split('/')])


def get_country(soup):
    all_h3 = soup.find_all('h3')
    return all_h3[-1].get_text()


def parse_utc_gmt(timezone):
    request = ''
    if timezone == 'UTC':
        request = 'https://www.zeitverschiebung.net/en/abbr/185'
    elif timezone == 'GMT':
        request = 'https://www.zeitverschiebung.net/en/abbr/89'
    responce = requests.get(request)
    soup = BeautifulSoup(responce.text)
    soup = soup.find_all(class_='light-gray-bg space-bottom clearfix')[1]

    result = []
    for link in soup.find_all('a'):
        small_tag = link.find_all('small')
        if not small_tag:
            continue
        result.append((link.find(text=True), small_tag[0].text))

    return result


if __name__ == '__main__':

    finally_list = []

    for timezone in pytz.common_timezones:
        print(timezone)
        current_tz = {'model': 'bot_app.timezone', 'fields': {}}

        if timezone in UNUSUAL_TZS.keys():
            current_tz['fields'] = UNUSUAL_TZS['fields']
            finally_list.append(current_tz.copy())
            continue

        if timezone == 'UTC' or timezone == 'GMT':
            for country, city in parse_utc_gmt(timezone):
                current_tz['fields'] = {'name': timezone, 'country': country, 'city': city}
                finally_list.append(current_tz.copy())
            continue

        
        country, cities = get_cities(timezone)
        for city in cities:
            current_tz['fields'] = {'name': timezone, 'country': country, 'city': city}
            finally_list.append(current_tz.copy())

    json_list = json.dumps(finally_list)

    with open('cities.json', 'w') as file:
        file.write(str(json_list))
